﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Windows.Forms;

namespace SimpleWCFBack
{  
    public class ServiceSimple : IServiceSimple
    {
        public ServiceSimple()
        {
            ConnectToDB();
        }

        SqlConnection conn;
        SqlCommand comm;
        SqlConnectionStringBuilder connStrBuilder;

        private void ConnectToDB()
        {
            connStrBuilder = new SqlConnectionStringBuilder();
            connStrBuilder.DataSource = "DESKTOP-73U4KUK";
            connStrBuilder.InitialCatalog = "simpleWCF";
            connStrBuilder.Encrypt = true;
            connStrBuilder.TrustServerCertificate = true;
            connStrBuilder.ConnectTimeout = 30;
            connStrBuilder.AsynchronousProcessing = true;
            connStrBuilder.MultipleActiveResultSets = true;
            connStrBuilder.IntegratedSecurity = true;

            conn = new SqlConnection(connStrBuilder.ToString());
            comm = conn.CreateCommand();
        }

        public int InsertUser(User user)
        {
            try
            {
                comm.CommandText = "INSERT INTO [User] (Username, Score) VALUES(@Username, @Score)";
                comm.Parameters.AddWithValue("Username", user.Username);
                comm.Parameters.AddWithValue("Score", user.Score);

                comm.CommandType = CommandType.Text;
                conn.Open();

                return comm.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public int UpdateUser(User user)
        {
            try
            {
                comm.CommandText = "UPDATE [User] SET Username=@Username, Score=@Score WHERE UserID=@UserID";
                comm.Parameters.AddWithValue("UserID", user.UserID);
                comm.Parameters.AddWithValue("Username", user.Username);
                comm.Parameters.AddWithValue("Score", user.Score);

                comm.CommandType = CommandType.Text;
                conn.Open();

                return comm.ExecuteNonQuery();
            }
            catch (Exception)
            {
               throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public int DeleteUser(int userID)
        {
            try
            {
                comm.CommandText = "DELETE FROM [User] WHERE UserID=@UserID";
                comm.Parameters.AddWithValue("UserID", userID);

                comm.CommandType = CommandType.Text;
                conn.Open();

                return comm.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public User GetUser(int userID)
        {
            User user = new User();

            try
            {
                comm.CommandText = "SELECT * FROM [User] WHERE UserID=@UserID";
                comm.Parameters.AddWithValue("UserID", userID);

                comm.CommandType = CommandType.Text;
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    user.UserID = Convert.ToInt32(reader[0]);
                    user.Username = reader[1].ToString();
                    user.Score = Convert.ToInt32(reader[2]);
                }

                return user;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        public List<User> GetTopUsers(int limit)
        {
            List<User> users = new List<User>();

            try
            {
                comm.CommandText = String.Format(
                   "SELECT TOP {0} * FROM [User] ORDER BY Score DESC", limit
                ); 
                
                comm.CommandType = CommandType.Text;
                conn.Open();

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    User user = new User()
                    {
                        UserID = Convert.ToInt32(reader[0]),
                        Username = reader[1].ToString(),
                        Score = Convert.ToInt32(reader[2])
                    };

                    users.Add(user);
                }

                return users;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
    }  
}
