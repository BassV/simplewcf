﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SimpleWCFBack
{
    [ServiceContract]
    public interface IServiceSimple
    {
        // Create a new user
        [OperationContract]
        int InsertUser(User user);

        // Update user unformation
        [OperationContract]
        int UpdateUser(User user);

        // Delete a user
        [OperationContract]
        int DeleteUser(int userID);

        // Get a user
        [OperationContract]
        User GetUser(int userID);

        // Get top users
        [OperationContract]
        List<User> GetTopUsers(int limit);
    }
}
