﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleWCFFront.ServiceReference1;
using SimpleWCFBack;

namespace SimpleWCFFront
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void InsertButton_Click(object sender, EventArgs e)
        {
            User user = new User();
            user.Username = UsernameInput.Text;
            user.Score = Convert.ToInt32(ScoreInput.Text);

            ServiceSimpleClient service = new ServiceSimpleClient();

            if (service.InsertUser(user) == 1)
            {
                MessageBox.Show("User added successfully");
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            User user = new User();
            user.UserID = Convert.ToInt32(UserIDInput.Text);
            user.Username = UsernameInput.Text;
            user.Score = Convert.ToInt32(ScoreInput.Text);

            ServiceSimpleClient service = new ServiceSimpleClient();

            if (service.UpdateUser(user) == 1)
            {
                MessageBox.Show("User updated successfully");
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(UserIDInput.Text);

            ServiceSimpleClient service = new ServiceSimpleClient();

            if (service.DeleteUser(userID) == 1)
            {
                MessageBox.Show("User deleted successfully");
            }
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(UserIDInput.Text);

            ServiceSimpleClient service = new ServiceSimpleClient();

            List<User> users = new List<User>() { service.GetUser(userID) };

            usersDGV.DataSource = users;
        }

        private void SelectTopButton_Click(object sender, EventArgs e)
        {
            List<User> users = new List<User>();

            ServiceSimpleClient service = new ServiceSimpleClient();

            usersDGV.DataSource = service.GetTopUsers(3);
        }
    }
}
