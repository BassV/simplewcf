# Create a new user
INSERT INTO [User] (Username, Score) VALUES('Jody', 88);

# Update a user information
UPDATE [User] SET Username='Rick', Score=92 WHERE UserID=7;

# Delete a user
DELETE FROM [User] WHERE UserID=4;

# Select a user
SELECT * FROM [User] WHERE userID=3;

# Select the top users
SELECT TOP 3 * FROM [User] ORDER BY Score DESC;